import flask
from flask import request
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import os
from pymongo import MongoClient
import logging

###
# Globals
###
app = flask.Flask(__name__)
app.secret_key = 'vadfvaegaetfbv'

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.brevtimes

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    # clear collection
    # db.brevtimes.drop()
    return flask.render_template('calc.html')

@app.route("/_times")
def _times():
    return flask.render_template('times.html')

@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found: {}".format(error))
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############

@app.route("/_calc_times")
def _calc_times():
    '''
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    '''
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    dist = request.args.get('dist', type=int)
    date = request.args.get('date', type=str)
    time = request.args.get('time', type=str)

    # Example: date='2017-01-01' time='00:00'
    date_and_time = arrow.get('{} {}'.format(date, time), 'YYYY-MM-DD H:mm')

    app.logger.debug("km={}".format(km))

    open_time = acp_times.open_time(km, dist, date_and_time)
    close_time = acp_times.close_time(km, dist, date_and_time)
    app.logger.debug("open: {} close: {}".format(open_time, close_time))

    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)

def errorCheck(miles, kms):
    """ Auxiliary function for submit button. This ensures that the entries
        are not empty, and it makes sure the entries do not contain a letter
        or space.
    """
    letters = " abscdfghijklmnopqrstuvwxyz"

    distances = [miles, kms]
    empty = 1

    # check for characters
    for distance in distances:
        for point in distance:
            if point != '':
                empty = 0
                point = point.lower()
                for letter in letters:
                    if letter in point:
                        return -1  # found a letter in either km or miles
    if empty:
        return 0  # km or miles are empty
    return 1  # all good, should save into database

@app.route('/_sub', methods=['POST', 'GET'])
def _submit():
    """ Submit button was pressed """
    app.logger.debug("Submit button was pressed")

    distance = request.args.get('distance', type=int)
    begin_date = request.args.get('begin_date', type=str)
    begin_time = request.args.get('begin_time', type=str)

    miles = request.args.getlist('miles', type=str)
    kms = request.args.getlist('km', type=str)
    locations = request.args.getlist('location', type=str)
    open_times = request.args.getlist('open', type=str)
    close_times = request.args.getlist('close', type=str)

    # check for errors in the km or miles lists
    err_val = errorCheck(miles, kms)
    if err_val == -1:
        msg = 'ERROR: You can only enter numbers for the controle distances!'
    elif err_val == 0:
        msg = 'ERROR: You must enter at least one controle distance before submitting'
    else:
        msg = 'Entries were successfully entered into the database'

        # clear collection
        db.brevtimes.drop()

        # add brevet info into database
        item_doc = {'distance': distance,
                    'begin_date': begin_date,
                    'begin_time': begin_time}
        db.brevtimes.insert_one(item_doc)

        # add the rest
        for i in range(len(miles)):
            if kms[i] != '':
                item_doc = {
                    "mile": miles[i],
                    "km": kms[i],
                    "location": locations[i],
                    "open": open_times[i],
                    "close": close_times[i]
                }
                db.brevtimes.insert_one(item_doc)

    result = {'msg': msg}
    return flask.jsonify(result=result)

@app.route('/_display', methods=['POST'])
def _display():
    """ display button pressed """
    app.logger.debug("Display button was pressed")
    msg = ""
    _items = db.brevtimes.find()
    items = []  # store the times
    info = {'distance': "", 'begin_date': "", "begin_time": ""}  # default

    for item in _items:
        if 'distance' in item:
            info = item  # get the brevet info (distance, start date and time)
        else:
            items.append(item)
    app.logger.debug("found in DB: {}".format(items))

    if len(items) == 0:
        msg = "ERROR: Nothing found from the database"

    err_msg = {"msg": msg}
    return flask.render_template('times.html', items=items, info=info, msg=err_msg)

#############

#app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
