# Project 5: Brevet time calculator with Ajax and MongoDB

Simple list of controle times from project 4 stored in MongoDB database.
Project 5 adds two buttons: "Submit" and "Display". The submit button will
save the current controle times into the database, and the display button
will retrieve the data and display them in a new page. 

Author: Jimmy Lam, jimmyl@uoregon.edu

## Using this program

Change your current directory to where docker-compose.yml is located.

To run everything, use the command
    
    docker-compose up
    
If this is the very first time running this, include the --build flag as follows

    docker-compose up --build
    
Now, go to a browser window, and go to localhost:5000 to access the brevet time calculator. 

## How the open and close times are calculated

This chart is taken from https://rusa.org/pages/acp-brevet-control-times-calculator

| Control location (km) | Minimum Speed (km/hr) | Maximum Speed (km/hr)|
|:---------------------:|:---------------------:|:--------------------:|
| 0 - 200               | 15                    | 34                   |
| 200 - 400             | 15                    | 32                   |
| 400 - 600             | 15                    | 30                   |
| 600 - 1000            | 11.428                | 28                   |
| 1000 - 1300           | 13.333                | 26                   |

Important: The overall time limit for each brevet, according to the distance, are
(in hours and minutes, HH:MM) 13:30 for 200 KM, 20:00 for 300 KM, 27:00 for 400 KM, 
40:00 for 600 KM, and 75:00 for 1000 KM.

The opening time is calculated as (distance/maximum speed) according to the control location range.
For example, a control point at 100km on a 200km brevet will have an opening time of 100/34 = 2.9412 hours = 2:56
past the opening time. For a control at 550km on a 600km brevet, the opening time will be
200/34 + 200/32 + 150/30 = 17.1324 hours = 17:08. Note how all the rows in the table up to the brevet distance
are used. For control distances greater than the brevet distance, use the brevet distance itself for the final 
calculation. For example, a control point at 405km on a 400km brevet should have an opening time of
200/34 + 200/32 = 12.1324 hours = 12:08.

The closing times are a little more tricky. 

For control points greater than or equal to 60km, the closing time is calculated as (distance/minimum speed)
in the same way as for the open times. For example, a control point at 550km on a 600km brevet would have a
closing time of 200/15 + 200/15 + 150/15 = 36.6667 hours = 36:40. For all controls greater than or equal to the
brevet distance, use the overall time limit given below the table above. For example, a control point at 205km on
a 200km brevet should close in 13:30.

For control points less than 60km, the minimum speed is based off of 20 km/hr plus 1 additional hour. For example,
a control at 20km would have a closing time of 20/20 + 1 = 2.0000 hours = 2:00 past the start time instead of 20/15
as in the table. 

## Test cases

The following are the different test cases developed to make sure the two buttons work. Note
that you can enter the test values into either the Miles or Km cell as it doesn't matter which
one is used. Also, either click off the current cell or press return to save the cell before 
clicking on submit. (You can tell it works if the open and close times are automatically populated)
All messages resulting from clicking either button will be shown below the table and above the submit button.

Submit button:

* Clicking the button when there are no times entered will result in the following message: 
"ERROR: You must enter at least one controle distance before submitting"

* If the user enters one or more space characters into a cell, the following message will be
displayed above the button: "ERROR: You can only enter numbers for the controle distances!". In
addition, the message "You can only enter numeric values!" will appear to the right of the row 
with the issue.

* If a cell contains one or more letters, then the following message will be displayed:
"ERROR: You can only enter numbers for the controle distances!"

* If the user enters a number then a letter, or letter than number, the same message as above
is shown.

* Otherwise, if the user enters a number (int or float), then the following message will
appear to inform the user the data was saved correctly: "Entries were successfully entered into the database".
Note that the previous data in the database is cleared before the current page data is stored.

* Pressing the button repeatedly, one after another, will repeatedly erase and overwrite the data
in the database if the input distances are valid. Otherwise, the error message will persist. Ultimately, 
nothing will change in the user's perspective.

Display button:

Note: clicking on this will redirect the user to a new page. Click the back button to return to the 
brevet page. 

* On the first time the page is loaded, (when the server is first started) clicking on the button will
result in a page with no values for the brevet distance, start date, or start time at the top, and the
message "ERROR: Nothing found from the database" will be displayed. 

* With no values stored in the database, clicking on the display button, back button, then the display button
again will show the same page as before, unchanged, because nothing has been changed.

* After data has been submitted at least once, the data will be displayed, one location per line, on the 
new page whenever the display button is pressed. Also, the top of the page will display the brevet distance,
start date, and start time.

* Clicking on the display button, back button, then display again without submitting anything new (but after the user
submitted the first time) will show the same page as before.

* If the user first clicks submit, closes the browser tab, goes back to localhost:5000, then presses
the display button without clicking submit first, the previously stored data in the database will
be displayed. This was a design choice. It is easy to just erase the database whenever a new page is 
loaded, but I chose to let the data persist.

* If the user has changed the distances for any row but did not press the submit button, pressing the
display button will show the previously saved data, not the current changes. Clicking on the back button 
in the browser should show the unsaved distances and times. 
